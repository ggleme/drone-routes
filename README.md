# Documentação Scripts Python e ShellScript


## Inspeção Externa de Aeronaves Suportada por Drone.

Este é um repositório para a documentação dos scripts escritos em Python e ShellScript utilizados para a execução da inspeção externa de Aeronave com o Drone Anafi Parrot.

![Drone Parrot Anafi](resources/anafi.png "Drone Parrot Anafi")

# Instalação do Parrot-Sphinx 

Parrot-Sphinx é uma ferramenta de simulação inicialmente pensada para cobrir as necessidades dos engenheiros da Parrot que desenvolvem software para drones. O conceito principal é executar o firmware do drone Parrot em um PC, em um ambiente isolado e bem separado do sistema host. O Gazebo é amplamente utilizado para simular o ambiente físico e visual do drone.

# Instrução para a instalação do Parrot-Sphinx no Ubuntu 18.04

Adicione o repositório padrão do Parrot-Sphinx em seu sistema:

```sh
$ echo "deb http://plf.parrot.com/sphinx/binary `lsb_release -cs`/" | sudo tee /etc/apt/sources.list.d/sphinx.list > /dev/null
$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 508B1AE5
```
Em seguinda siga com a instalação do pacote conforme demonstrado abaixo:

```sh
$ sudo apt update
$ sudo apt install parrot-sphinx
```
# Primeiros passos com Parrot-Sphinx

Para a execução do simulador é necessário iniciar o firmware service. 

```sh
$ sudo systemctl start firmwared.service
```

Iremos utilizar o comando fdc para verifique se o mesmo foi iniciado, a resposta aguarda é PONG que informa a inicialização correta do firmware:

```sh
$ fdc ping
PONG
```

Para seguir os próximos passos analise o nome da sua interface de rede wifi:

```sh
$ iwconfig
```

Este passo é necessário para uma confirmação que sua interface está com o nome de "wlan0" pois para a execução do arquivo .drone é necessário está especificação.

#Inicie sua primeira simulação

Agora que o daemon firmwared está em execução, basta escolher um arquivo .drone e iniciar o Parrot-Sphinx com ele. Vários arquivos .drone são fornecidos junto com a instalação do Parrot-Sphinx. Eles estão localizados em /opt/parrot-sphinx/usr/share/sphinx/drones/. Vamos iniciar o Parrot-Sphinx com um deles. Se o nome da sua interface wifi for “wlan0”, digite o seguinte comando para iniciar o Parrot-Sphinx:

```sh
$ sphinx /opt/parrot-sphinx/usr/share/sphinx/drones/anafi4k.drone
```
Caso sua interface de wifi tenha outra nome execute o seguinte comando:

```sh
$ sphinx /opt/parrot-sphinx/usr/share/sphinx/drones/anafi4k.drone::stolen_interface=<nome_da_sua_interface>:eth0:192.168.42.1/24
```

Como é a primeira vez que o Parrot-Sphinx é iniciado com esse arquivo .drone, pode demorar alguns segundos para baixar o firmware do drone do servidor externo. Depois que o firmware é carregado, a simulação inicia e você deve ver algo como abaixo.

![Simulador Parrot-Sphinx](resources/primeiro_passo.png "Simulador Parrot-Sphinx")

Parabéns seu simulador Parrot-Sphinx está funcionando corretamente.

# Instalação do Olympe versão 1.2.1

O Olympe fornece uma interface de programação de controlador Python para o Parrot Drone. Isso significa que você pode usar o Olympe para conectar e controlar um drone a partir de um script Python remoto executado no seu computador. O Olympe se destina principalmente ao uso com um drone simulado (usando o Sphinx, o simulador de drones Parrot), mas também pode ser usado para conectar-se a drones físicos. Como o GroundSDK-iOS e o GroundSDK-Android, o Olympe é baseado em arsdk-ng / arsdk-xml.

# Instrução para a instalação do Olympe no Ubuntu 18.04

Clone do repositório parrot-groundsdk

```sh
cd $HOME
mkdir -p code/parrot-groundsdk
cd code/parrot-groundsdk
pwd
repo init -u https://github.com/Parrot-Developers/groundsdk-manifest.git
repo sync
```

Instalação das dependências do Olympe para Linux

```sh
# pdraw dependencies
$ sudo apt-get -y install build-essential yasm cmake libtool libc6 libc6-dev \
  unzip freeglut3-dev libglfw3 libglfw3-dev libsdl2-dev libjson-c-dev \
  libcurl4-gnutls-dev libavahi-client-dev libgles2-mesa-dev

# ffmpeg build dependencies
$ sudo apt-get -y install rsync

# arsdk build dependencies
$ sudo apt-get -y install cmake libbluetooth-dev libavahi-client-dev \
    libopencv-dev libswscale-dev libavformat-dev \
    libavcodec-dev libavutil-dev cython python-dev

# olympe build dependency
$ pip3 install clang

```
Chegou a hora de realizar o build da aplicação no Linux:

```sh
$ pwd
~/code/parrot-groundsdk
$ ./build.sh -p olympe-linux -A all final -j
```
Realizando o build corretamente sua instalação chega ao fim e o Olympe está pronto para ser utilizado. Para realizar o teste que eles foi instalado corretamente siga as etapas abaixo:

```sh
$ pwd
~/code/parrot-groundsdk
$ source ./products/olympe/linux/env/shell
(olympe-python3) $ python -c 'import olympe; print("Installation OK")'
$ exit
```


## Exemplo de uso manual da função de inspeção..

Para reproduzir de forma manual é necessário a seguinte execução:

### Passo 1 - Clonar o repositário :

```sh
$ git clone https://bitbucket.org/ggleme/drone-routes.git
```

Após realizar o clone do repositório acessa a pasta do projeto, conforme demonstrado abaixo:

```sh
$ cd drone-routes/src
```

### Passo 2 - Executar o arquivo :

**Shell Script**
```sh
$ ./start_inspect.sh 
```
Está execução irá carregar as váriaveis de ambiente necessárias, e iniciar o comando de inspeção no drone.

## Meta

Fábio A. Ferreira         – fabio.ferreira@ga.ita.br

Gustavo de Godoi Leme     – gustavo.leme@ga.ita.br

Carlos Eduardo Scussiatto – carlos.scussiatto@ga.ita.br

Iago Oliveira Lima        – iago.lima@ga.ita.br

