# -*- coding: UTF-8 -*-

import olympe
import time, math


from olympe.messages.ardrone3.Piloting import TakeOff, moveBy, Landing, moveTo, Circle, PCMD
from olympe.messages.ardrone3.PilotingState import FlyingStateChanged
from olympe.messages.ardrone3.PilotingState import moveToChanged, FlyingStateChanged, PositionChanged, AttitudeChanged
from olympe.messages.ardrone3.GPSSettingsState import GPSFixStateChanged
from olympe.messages.ardrone3.PilotingState import GpsLocationChanged
from olympe.enums.ardrone3.Piloting import MoveTo_Orientation_mode

olympe.log.update_config({"loggers": {"olympe": {"level": "WARNING"}}})


RAD = (2.0 * math.pi)/360
DRONE_IP = "10.202.0.1"

def main():
    drone = olympe.Drone(DRONE_IP)

    drone.connect()

    # Start a takeoff for inspection
    takeoffAction = drone(
        TakeOff()
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> GPSFixStateChanged(fixed=1, _timeout=10, _policy="check_wait")
    )
    if not takeoffAction.wait().success():
        assert False, "Cannot complete the TAKEOFF action"
    #time.sleep(5)

    # Get the home position
    drone_location = drone.get_state(GpsLocationChanged)

    # Start a flying for inspection
    flyingAction = drone(
        moveBy(0, 0, 0, 145.0*RAD)
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> moveBy(0, 0, -4.0, 0)
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> moveBy(17.2, 0, 0, 0)
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> moveBy(0, 0, 0, 35.0*RAD)
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> moveBy(0, 22, 0, 0)
        >> FlyingStateChanged(state="hovering", _timeout=35)
        >> moveBy(0, 0, -2, 0)
        >> FlyingStateChanged(state="hovering", _timeout=35)

#        >> moveBy(0, 0, 0, -140.0*RAD)
#        >> FlyingStateChanged(state="hovering", _timeout=35)
#        >> moveBy(17.5, 0, 0, 0)
#        >> FlyingStateChanged(state="hovering", _timeout=35)
    )

    if not flyingAction.wait().success():
        assert False, "Cannot complete the FLYING action"
    
    # Go home and shutdown
    gohomeAction = drone(
        moveTo(drone_location["latitude"],  drone_location["longitude"], drone_location["altitude"], MoveTo_Orientation_mode.TO_TARGET, 0.0)
        >> FlyingStateChanged(state="hovering", _timeout=5)
        >> moveToChanged(latitude=drone_location["latitude"], longitude=drone_location["longitude"], altitude=drone_location["altitude"], orientation_mode=MoveTo_Orientation_mode.TO_TARGET, status='DONE', _policy='wait')
        >> FlyingStateChanged(state="hovering", _timeout=5)
        >> Landing()
    )
    if not gohomeAction.wait().success():
        assert False, "Cannot complete the SHUTDOWN action"

    drone.disconnect()




if __name__ == "__main__":
    main()
