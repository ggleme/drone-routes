#!/usr/bin/env bash

################################################################################
# Copyright (C) 2020 - CE-289			                               #
#                                                                              #
# Este trabalho está licenciado sob uma Licença Creative Commons               #
# Atribuição-Compartilhamento pela mesma Licença 2.5 Brasil. Para ver a copia  #
# desta licença, acesse: http://creativecommons.org/licenses/by-sa/2.5/br/     #
# ou envie uma carta para Creative Commons, 171 Second Street, Suite 300,      #
# San Francisco, California 94105, USA.                                        #
#                                                                              #
################################################################################
#
# Versão 1.0
#

# == FUNCOES ==

_setEnv () {

echo 
echo "=============================================================================="
echo " APLICANDO AS VARIAVEIS DE AMBIENTE                                           "
echo "=============================================================================="
echo 

export PATH_SCRIPT=/opt/code

export OLYMPE_DIR_INSTALL=/opt/code/parrot-groundsdk

export OLYMPE_XML=$OLYMPE_DIR_INSTALL/out/olympe-linux/staging-host/usr/lib/arsdkgen/xml

export OLYMPE_GENERATE=$OLYMPE_DIR_INSTALL/packages/olympe/src/olympe/

export OLYMPE_LIB_PATH=$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib

export PYTHONPATH=$OLYMPE_DIR_INSTALL/packages/olympe/src:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib/python:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib/python/site-packages:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/local/lib/python:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/local/lib/python/site-packages:$OLYMPE_DIR_INSTALL/out/olympe-linux/staging-host/usr/lib/arsdkgen:/$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib/python/site-packages/:/$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib/python/site-packages/

export LD_LIBRARY_PATH=$OLYMPE_DIR_INSTALL/out/olympe-linux/final/lib:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/lib:PATH=$OLYMPE_DIR_INSTALL/out/olympe-linux/final/bin:$OLYMPE_DIR_INSTALL/out/olympe-linux/final/usr/bin:$OLYMPE_DIR_INSTALL/.python//py3/bin:$OLYMPE_DIR_INSTALL/.python/py3/bin:$HOME/.nvm/versions/node/v12.18.1/bin:$HOME/.local/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/snap/bin

}

_startInsp () {

echo 
echo "=============================================================================="
echo " INICIANDO A INSPECÃO ATRAVÉS DO DRONE                                        "
echo "=============================================================================="
echo 

python $PATH_SCRIPT/inspectGPS.py

}


# == CHAMADAS DAS FUNCOES ==

_setEnv
_startInsp


